import pytest
import requests


@pytest.mark.one
def test_method1():
    r = requests.get('https://the-internet.herokuapp.com/context_menu')
    assert ("Right-click in the box below to see one called 'the-internet'") in r.text


@pytest.mark.two
def test_method2():
    r = requests.get('https://the-internet.herokuapp.com/context_menu')
    assert ("Alibaba") in r.text
