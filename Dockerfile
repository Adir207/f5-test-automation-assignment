FROM python:3

WORKDIR /code
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY public/ .

CMD [ "pytest", "./main.py" ]
